SET client_encoding = 'UTF8';
-- Создание таблиц
CREATE TABLE IF NOT EXISTS employees (
  id serial PRIMARY KEY,
  firstname text NOT NULL,
  lastname text NOT NULL,
  patronymic text,
  degree text,
  address text,
  email text,
  inter_phone VARCHAR(4),
  phone VARCHAR(16),
  photo BYTEA
);

CREATE TABLE IF NOT EXISTS departments (
  number int PRIMARY KEY,
  parent_department_number int REFERENCES departments (number),
  name text NOT NULL,
  inter_phone VARCHAR(4),
  phone VARCHAR(16),
  address text NOT NULL,
  email VARCHAR(40),
  url text
);

CREATE TABLE IF NOT EXISTS positions (
  code serial PRIMARY KEY,
  department_number int REFERENCES departments (number),
  name text NOT NULL
);

CREATE TABLE IF NOT EXISTS employees_positions (
  employee_id int REFERENCES employees (id),
  position_code int REFERENCES positions (code)
);

CREATE TABLE IF NOT EXISTS roles (
  id smallserial PRIMARY KEY,
  name VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS users (
  id serial PRIMARY KEY,
  role_id SMALLINT NOT NULL REFERENCES roles (id),
  login VARCHAR(30) UNIQUE NOT NULL,
  password VARCHAR(256) NOT NULL
);

-- Представления
create view employees_fullname as SELECT id, lastname || ' ' || firstname || ' ' || patronymic as fullname, degree, address, email, inter_phone, phone FROM employees;

-- Функции
CREATE FUNCTION find_user(lgn varchar) RETURNS users AS $$
	SELECT users.id, users.role_id, users.login, users.password
	FROM users
	WHERE users.login = lgn
	LIMIT 1;
$$ LANGUAGE SQL SECURITY DEFINER; 

-- Создание ролей и выдача прав

CREATE ROLE user_role;
grant select on departments to user_role;
grant select on employees to user_role;
grant select on employees_positions to user_role;
grant select on positions to user_role;
grant select on employees_fullname to user_role;
grant execute on function find_user to user_role;

CREATE ROLE admin_role;
grant select, insert, update, delete on departments to admin_role;
grant select, insert, update, delete on employees to admin_role;
grant select, insert, update, delete on employees_positions to admin_role;
grant select, insert, update, delete on positions to admin_role;
grant select, insert on roles to admin_role;
grant select, insert, update, delete on users to admin_role;
grant select on employees_fullname to admin_role;
grant usage, select on all sequences in schema public to admin_role;
grant execute on function find_user to admin_role;

-- Роли
INSERT INTO roles VALUES (1, 'пользователь');
INSERT INTO roles VALUES (2, 'администратор');

-- Пользователи
CREATE USER usr WITH PASSWORD '7ee4c3ec0b4ee3b9fd76755e03fc705c96866c89a610ccf10080a37899744a5b';
CREATE USER admin WITH password '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918' CREATEDB;
INSERT INTO users (role_id, login, password) VALUES (1, 'usr', '7ee4c3ec0b4ee3b9fd76755e03fc705c96866c89a610ccf10080a37899744a5b');
INSERT INTO users (role_id, login, password) VALUES (2, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918');

GRANT user_role TO usr;
GRANT admin_role TO admin;

-- Департаменты
INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) VALUES (1, null, 'Факультет вычислительных систем (ФВС)', '2814', '(3822) 90-01-96', '634034, Томская область, Томск, ул. Вершинина, 74 (фэт), Офис: 203', null, 'https://tusur.ru/ru/o-tusure/struktura-i-organy-upravleniya/departament-obrazovaniya/fakultety-i-kafedry/fakultet-vychislitelnyh-sistem');
INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) VALUES (6, 1, 'Кафедра компьютерных систем в управлении и проектировании (КСУП)', null, '(3822) 41-47-17', '634034, Томская область, Томск, ул. Вершинина, 74 (фэт), Офис: 322', 'office@kcup.tusur.ru', 'http://www.kcup.tusur.ru/');
INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) VALUES (7, 1, 'Кафедра механики и графики (МиГ)', '2500', '(3822) 41-39-40', '634034, Томская область, Томск, ул. Вершинина, 47 (радиотехнический), Офис: 505', 'mguk@tusur.ru', 'https://tusur.ru/ru/o-tusure/struktura-i-organy-upravleniya/departament-obrazovaniya/fakultety-i-kafedry/fakultet-vychislitelnyh-sistem/kafedra-mehaniki-i-grafiki-mig');
INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) VALUES (8, 1, 'Кафедра экономической математики, информатики и статистики (ЭМИС)', '2811', '(3822) 90-01-87', '634034, Томская область, Томск, ул. Вершинина, 74 (фэт), Офис: 422', 'emis@tusur.ru', 'https://tusur.ru/ru/o-tusure/struktura-i-organy-upravleniya/departament-obrazovaniya/fakultety-i-kafedry/fakultet-vychislitelnyh-sistem/kafedra-ekonomicheskoy-matematiki-informatiki-i-statistiki');
INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) VALUES (2, null, 'Факультет систем управления (ФСУ)', '2055', '(3822) 70-15-46', '634034, Томская область, Томск, ул. Вершинина, 74 (фэт), Офис: 410', null, 'https://tusur.ru/ru/o-tusure/struktura-i-organy-upravleniya/departament-obrazovaniya/fakultety-i-kafedry/fakultet-sistem-upravleniya');
INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) VALUES (3, null, 'Факультет безопасности (ФБ)', null, '(3822) 70-15-29', '634034, Томская область, Томск, Красноармейская, 146 (учебно-лабораторный корпус), Офис: 510', 'dem@keva.tusur.ru', 'http://fb.tusur.ru/');
INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) VALUES (4, 3, 'Кафедра комплексной информационной безопасности электронно-вычислительных систем (КИБЭВС)', null, '(3822) 70-15-29', '634034, Томская область, Томск, Красноармейская, 146 (учебно-лабораторный корпус), Офис: 515', 'dem@keva.tusur.ru', 'https://tusur.ru/ru/o-tusure/struktura-i-organy-upravleniya/departament-obrazovaniya/fakultety-i-kafedry/fakultet-bezopasnosti/kafedra-kompleksnoy-informatsionnoy-bezopasnosti-elektronno-vychislitelnyh-sistem');
INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) VALUES (5, 3, 'Кафедра безопасности информационных систем (БИС)', null, null, '634034, Томская область, Томск, Красноармейская, 146 (учебно-лабораторный корпус), Офис: 508', 'etf@fb.tusur.ru', null);

-- Должности
INSERT INTO positions (department_number, name) VALUES (4, 'Заведующий кафедрой');
INSERT INTO positions (department_number, name) VALUES (4, 'Доцент');
INSERT INTO positions (department_number, name) VALUES (4, 'Старший преподаватель');
INSERT INTO positions (department_number, name) VALUES (4, 'Профессор');
INSERT INTO positions (department_number, name) VALUES (4, 'Преподаватель');
INSERT INTO positions (department_number, name) VALUES (4, 'Программист');
INSERT INTO positions (department_number, name) VALUES (5, 'И.о. заведующего кафедрой');
INSERT INTO positions (department_number, name) VALUES (5, 'Доцент');
INSERT INTO positions (department_number, name) VALUES (5, 'Инженер');
INSERT INTO positions (department_number, name) VALUES (5, 'Старший преподаватель');
INSERT INTO positions (department_number, name) VALUES (5, 'Техник');
INSERT INTO positions (department_number, name) VALUES (5, 'Ассистент');


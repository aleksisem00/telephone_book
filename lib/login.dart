import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/database/database.dart';
import 'package:school_app/book/database/repos/users.dart';
import 'package:school_app/storage.dart';
import 'package:school_app/user/role.dart';
import 'package:school_app/user/user.dart';

class LoginForm extends StatefulWidget {
  final Function() changeUser;
  const LoginForm({Key? key, required this.changeUser}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _loginTextBox = TextEditingController();
  final _passwordTextBox = TextEditingController();

  bool _showPassword = false;

  void userEnter() async {
    if (!_validateInputs()) return;
    var user = await UsersRepository.find(_loginTextBox.text);
    if (!_validatePassword(_passwordTextBox.text, user['password'])) return;
    List<Role> roles = [Role.user, Role.admin];
    Storage.setUser(User(user['login'], roles[user['role_id'] - 1]));
    Database.reconnect(user['login'], user['password']).then((value) {
      widget.changeUser();
    });
  }

  bool _validateInputs() {
    return (_loginTextBox.text.trim().isNotEmpty &&
        _passwordTextBox.text.trim().isNotEmpty);
  }

  bool _validatePassword(String password, String savedPassword) {
    String p = sha256.convert(utf8.encode(password)).toString();
    return (p == savedPassword);
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldPage(
      header: const Text('Вход'),
      content: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormBox(
              controller: _loginTextBox,
              header: 'Логин',
            ),
            TextFormBox(
              controller: _passwordTextBox,
              header: 'Пароль',
              obscureText: !_showPassword,
              suffix: IconButton(
                icon: Icon(
                    !_showPassword ? FluentIcons.lock : FluentIcons.unlock),
                onPressed: () => setState(() => _showPassword = !_showPassword),
              ),
            ),
            FilledButton(
                child: const Text('Войти'), onPressed: () => userEnter())
          ],
        ),
      ),
    );
  }
}

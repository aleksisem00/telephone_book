import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/database/entities/department.dart';
import 'package:school_app/book/database/repos/departments.dart';

class DepartmentAddForm extends StatefulWidget {
  const DepartmentAddForm({Key? key}) : super(key: key);

  @override
  State<DepartmentAddForm> createState() => _DepartmentAddFormState();
}

class _DepartmentAddFormState extends State<DepartmentAddForm> {
  late List<Department>? _departments;

  final _controller = ScrollController(keepScrollOffset: true);
  final _departmentName = TextEditingController();
  final _address = TextEditingController();
  final _interPhone = TextEditingController();
  final _phone = TextEditingController();
  final _email = TextEditingController();
  final _url = TextEditingController();

  Department? comboBoxDepartment;

  @override
  void initState() {
    loadDepartments();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ContentDialog(
      title: const Text('Новое подразделение'),
      constraints: const BoxConstraints(maxWidth: 800, maxHeight: 800),
      content: ScaffoldPage(
        content: SingleChildScrollView(
          controller: _controller,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Родительское подразделение'),
              Combobox<Department?>(
                placeholder: const Text('Нет'),
                isExpanded: true,
                items: _departments!
                    .map((e) => ComboboxItem<Department?>(
                          value: e,
                          child: Text(e.name),
                        ))
                    .toList(),
                value: comboBoxDepartment,
                onChanged: (value) {
                  comboBoxDepartment = null;
                  if (value != null) chooseDepartment(value);
                },
                icon: const Icon(FluentIcons.add),
              ),
              TextFormBox(
                controller: _departmentName,
                header: 'Название подразделения',
                placeholder: 'Кафедра ...',
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (text) {
                  if (text == null || text.isEmpty)
                    return 'Введите название департамента';
                  return null;
                },
              ),
              TextFormBox(
                controller: _address,
                header: 'Адрес',
                placeholder: 'ул. Ленина 40',
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (text) {
                  if (text == null || text.isEmpty) return 'Введите адрес';
                  return null;
                },
              ),
              TextFormBox(
                controller: _interPhone,
                header: 'Внутр.',
                placeholder: '2222',
              ),
              TextFormBox(
                controller: _phone,
                header: 'Телефон',
                placeholder: '(222) 2222-22-22',
              ),
              TextFormBox(
                controller: _email,
                header: 'Email',
                placeholder: 'tst@tusur.ru',
              ),
              TextFormBox(
                controller: _url,
                header: 'Сайт',
                placeholder: 'https://tusur.ru',
              ),
            ],
          ),
        ),
      ),
      actions: [
        Button(
          child: const Text('Отмена'),
          onPressed: () => Navigator.pop(context),
        ),
        FilledButton(
          child: const Text('Создать'),
          onPressed: () => addDepartment(),
        ),
      ],
    );
  }

  void chooseDepartment(Department? value) {
    setState(() {
      comboBoxDepartment = value;
    });
  }

  void loadDepartments() async {
    var departments = await DepartmentsRepository.all();
    List<Department> departmentsList = departments
        .map((d) => Department(
            d['']!['number'],
            d['']!['parent_department_number'],
            d['']!['name'],
            'null',
            'null',
            'null',
            'null',
            'null'))
        .toList();
    departmentsList.insert(0,
        Department(-1, null, 'Нет', null, null, 'ул. Ленина 40', null, null));
    setState(() {
      _departments = departmentsList;
    });
  }

  void addDepartment() async {
    if (_departmentName.text.isNotEmpty &&
        comboBoxDepartment != null &&
        _address.text.isNotEmpty) {
      DepartmentsRepository.add(Department(
              0,
              (comboBoxDepartment!.number == -1)
                  ? null
                  : comboBoxDepartment!.number,
              _departmentName.text,
              _interPhone.text,
              _phone.text,
              _address.text,
              _email.text,
              _url.text))
          .then((value) {
        Navigator.pop(context);
        showSnackbar(context,
            const Snackbar(content: Text('Подразделение было добавлено')));
      });
    }
  }
}

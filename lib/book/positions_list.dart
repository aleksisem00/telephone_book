import 'package:fluent_ui/fluent_ui.dart';

class PositionsList extends StatefulWidget {
  final List<dynamic> positionsChosen;
  final Function(List<dynamic>) removePosition;
  const PositionsList(
      {Key? key, required this.positionsChosen, required this.removePosition})
      : super(key: key);

  @override
  State<PositionsList> createState() => _PositionsListState();
}

class _PositionsListState extends State<PositionsList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Column(
          children: widget.positionsChosen
              .map(
                (e) => Container(
                  padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  margin: EdgeInsets.symmetric(vertical: 4),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.grey[20],
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: 150,
                        child: Text(e[1]['positions']['name']),
                      ),
                      Expanded(child: Text(e[0]['']['name'])),
                      IconButton(
                          icon: const Icon(FluentIcons.delete),
                          onPressed: () => widget.removePosition(e)),
                    ],
                  ),
                ),
              )
              .toList()),
    );
  }
}

import 'dart:convert';
import 'dart:typed_data';

import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/database/entities/department.dart';
import 'package:school_app/book/database/entities/employee.dart';
import 'package:school_app/book/database/entities/position.dart';
import 'package:school_app/book/database/repos/employees.dart';
import 'package:school_app/book/employee_delete.dart';
import 'package:school_app/book/employee_edit.dart';
import 'package:school_app/storage.dart';
import 'package:school_app/user/role.dart';
import 'package:school_app/user/user.dart';

class EmployeeDetailedPage extends StatefulWidget {
  final int employeeID;
  const EmployeeDetailedPage({Key? key, required this.employeeID})
      : super(key: key);

  @override
  State<EmployeeDetailedPage> createState() => _EmployeeDetailedPageState();
}

class _EmployeeDetailedPageState extends State<EmployeeDetailedPage> {
  late Employee _employee;
  Image? _photo;

  final User user = Storage.getUser();

  @override
  void initState() {
    loadEmployee();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return NavigationView(
      appBar: const NavigationAppBar(
        title: Text('Сотрудник'),
      ),
      content: ScaffoldPage(
        content: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (user.role == Role.admin)
                CommandBar(
                  primaryItems: [
                    CommandBarButton(
                      icon: const Icon(FluentIcons.edit),
                      label: const Text('Изменить'),
                      onPressed: () => showDialog(
                              context: context,
                              builder: (context) =>
                                  EmployeeEditForm(employee: _employee))
                          .then((value) => loadEmployee()),
                    ),
                    CommandBarButton(
                      icon: const Icon(FluentIcons.delete),
                      label: const Text('Удалить'),
                      onPressed: () => showDialog(
                              context: context,
                              builder: (context) =>
                                  EmployeeDeleteDialog(employee: _employee))
                          .then((value) {
                        if (value == null) return;
                        Navigator.of(context).pop();
                        showSnackbar(
                            context,
                            Snackbar(
                                content: Text(
                                    'Сотрудник ${_employee.lastname} ${_employee.firstname} ${_employee.patronymic} был удалён')));
                      }),
                    ),
                  ],
                ),
              EmployeeInfo(employeeData: _employee, photo: _photo),
              if (_employee.positions.length > 1)
                DepartmentsList(
                  positions: _employee.positions,
                ),
            ],
          ),
        ),
      ),
    );
  }

  void loadEmployee() async {
    var employee = await EmployeesRepository.getOne(widget.employeeID);
    var positions = employee
        .map((e) => [
              Position(
                  e['positions']!['code'],
                  e['positions']!['department_number'],
                  e['positions']!['name']),
              Department(
                  e['departments']!['number'],
                  e['departments']!['parent_department_number'],
                  e['departments']!['name'],
                  e['departments']!['inter_phone'],
                  e['departments']!['phone'],
                  e['departments']!['address'],
                  e['departments']!['email'],
                  e['departments']!['url'])
            ])
        .toList();
    Uint8List? imageData = (employee[0]['']!['encode'] != null)
        ? Uint8List.fromList((employee[0]['']!['encode'] as String)
            .substring(1, employee[0]['']!['encode'].length - 1)
            .split(',')
            .map(int.parse)
            .toList())
        : null;
    Employee e = Employee(
        employee[0]['employees']!['id'],
        employee[0]['employees']!['firstname'],
        employee[0]['employees']!['lastname'],
        employee[0]['employees']!['patronymic'],
        employee[0]['employees']!['degree'],
        employee[0]['employees']!['address'],
        employee[0]['employees']!['email'],
        employee[0]['employees']!['inter_phone'],
        employee[0]['employees']!['phone'],
        positions,
        imageData);
    Image? photo;
    if (e.photo != null) {
      photo = Image.memory(imageData!);
    }
    setState(() {
      _employee = e;
      _photo = photo;
    });
  }
}

class EmployeeInfo extends StatelessWidget {
  final Employee employeeData;
  final Image? photo;
  const EmployeeInfo(
      {Key? key, required this.employeeData, required this.photo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        if (photo != null)
          SizedBox(
            width: 250,
            height: 300,
            child: Image.memory(employeeData.photo!),
          ),
        Container(
          child: Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${employeeData.lastname} ${employeeData.firstname} ${employeeData.patronymic}',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                ),
                SizedBox(
                  height: 8,
                ),
                Text((employeeData.positions[0][0] as Position).name,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
                Text(
                  (employeeData.positions[0][1] as Department).name,
                  style: TextStyle(fontSize: 17),
                ),
                SizedBox(
                  height: 8,
                ),
                if (employeeData.degree != null &&
                    employeeData.degree!.isNotEmpty) ...[
                  Text(
                    employeeData.degree!,
                    style: TextStyle(color: Colors.grey[100]),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                ],
                if (employeeData.address != null &&
                    employeeData.address!.isNotEmpty)
                  Text('Адрес: ${employeeData.address}'),
                if (employeeData.phone != null &&
                    employeeData.phone!.isNotEmpty)
                  Text('Тел.: ${employeeData.phone}'),
                if (employeeData.interPhone != null &&
                    employeeData.interPhone!.isNotEmpty)
                  Text('Внутр.: ${employeeData.interPhone}'),
                if (employeeData.email != null &&
                    employeeData.email!.isNotEmpty)
                  Text('Email: ${employeeData.email}'),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class DepartmentsList extends StatelessWidget {
  final List<dynamic> positions;
  const DepartmentsList({Key? key, required this.positions}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Text(
            'Другие должности:',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          SizedBox(height: 8),
          ...positions
              .sublist(1, positions.length)
              .map(
                (e) => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(e[1].name, style: TextStyle(fontSize: 16)),
                    Text(e[0].name,
                        style:
                            TextStyle(fontSize: 14, color: Colors.grey[150])),
                    SizedBox(
                      height: 8,
                    )
                  ],
                ),
              )
              .toList(),
        ],
      ),
    );
  }
}

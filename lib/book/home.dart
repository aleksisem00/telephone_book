import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/database/repos/departments.dart';
import 'package:school_app/book/database/repos/employees.dart';
import 'package:school_app/book/department.dart';
import 'package:school_app/book/department_add.dart';
import 'package:school_app/book/employees.dart';
import 'package:school_app/storage.dart';
import 'package:school_app/user/role.dart';
import 'package:school_app/user/user.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late List<Map<String, dynamic>> _departments;

  late User user = Storage.getUser();

  List<Map<String, Map<String, dynamic>>> _employees = [];
  TextEditingController searchController = TextEditingController();
  FocusNode searchFocusNode = FocusNode();

  void searchEmployee(String param) async {
    var employees = await EmployeesRepository.find(param, 0, 0);
    setState(() {
      _employees = employees;
    });
  }

  void loadEmployees() async {
    var employees = await EmployeesRepository.all(0, 0);
    setState(() {
      _employees = employees;
    });
  }

  void loadDepartments() async {
    var departments = await DepartmentsRepository.all();
    setState(() {
      _departments = departments;
    });
  }

  @override
  void initState() {
    loadDepartments();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    user = Storage.getUser();
    return ScaffoldPage(
      header: const Text('Главная'),
      content: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            const SizedBox(height: 8),
            if (user.role == Role.admin)
              CommandBar(primaryItems: [
                CommandBarButton(
                    icon: const Icon(FluentIcons.add),
                    label: const Text('Добавить подразделение'),
                    onPressed: () => showDialog(
                            context: context,
                            builder: (context) => const DepartmentAddForm())
                        .then((value) => loadDepartments())),
              ]),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: TextBox(
                    placeholder:
                        'ФИО, должность, подразделение, адрес, контактная информация',
                    suffix: IconButton(
                      icon: const Icon(FluentIcons.search),
                      onPressed: () {
                        if (searchController.text.isEmpty) {
                          return setState(() {
                            _employees = [];
                          });
                        }
                        searchEmployee(searchController.text);
                        searchFocusNode.requestFocus();
                      },
                    ),
                    controller: searchController,
                    onSubmitted: (text) {
                      if (text.isEmpty) {
                        return setState(() {
                          _employees = [];
                        });
                      }
                      searchEmployee(text);
                      searchFocusNode.requestFocus();
                    },
                  ),
                ),
              ],
            ),
            (_employees.isEmpty)
                ? DepartmentsTree(departments: _departments)
                : Expanded(
                    child: EmployeesList(
                      employees: _employees.map((e) => e['']!).toList(),
                      updateEmployeesList: loadEmployees,
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}

class DepartmentsTree extends StatefulWidget {
  final List<Map<String, dynamic>> departments;
  const DepartmentsTree({Key? key, required this.departments})
      : super(key: key);

  @override
  State<DepartmentsTree> createState() => _DepartmentsTreeState();
}

class _DepartmentsTreeState extends State<DepartmentsTree> {
  late List<TreeViewItem> _departmentsTree;

  void buildDepartmentsTree() async {
    buildDepartmentsTree();
    setState(() {
      _departmentsTree = dfs(widget.departments, 1, 0);
    });
  }

  @override
  Widget build(BuildContext context) {
    buildDepartmentsTree();
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      TreeView(
          items: _departmentsTree,
          onItemInvoked: (item) => Navigator.of(context).push(
                FluentPageRoute(
                  builder: (context) => DepartmentPage(
                    departmentID: item.value,
                  ),
                ),
              )),
    ]);
  }

  List<TreeViewItem> dfs(
      List<Map<String, dynamic>> list, int level, int index) {
    List<TreeViewItem> items = [];
    List<TreeViewItem> children = [];
    int i = index;
    while (i < list.length) {
      if (i < list.length - 1 && list[i + 1]['']['path'].length > level) {
        children = dfs(list, level + 1, i + 1);
        items.add(
          TreeViewItem(
            content: Text(list[i]['']['name']),
            children: children,
            value: list[i]['']['number'],
          ),
        );
        i += children.length;
      } else if (list[i]['']['path'].length < level) {
        break;
      } else {
        items.add(TreeViewItem(
          content: Text(list[i]['']['name']),
          children: [],
          value: list[i]['']['number'],
        ));
      }
      i++;
    }
    return items;
  }
}

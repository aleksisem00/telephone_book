import 'package:school_app/book/database/database.dart';
import 'package:school_app/book/database/entities/department.dart';

class DepartmentsRepository {
  /// Возвращает дерево всех подразделений
  static Future<List<Map<String, Map<String, dynamic>>>> all() async {
    return await Database.connection.mappedResultsQuery(
        "WITH RECURSIVE tree(number, parent_department_number, path) AS (SELECT number, parent_department_number, array[number]::integer[], name FROM departments WHERE parent_department_number IS NULL UNION ALL SELECT departments.number, departments.parent_department_number, path || array[departments.number]::integer[], departments.name FROM departments INNER JOIN tree on tree.number = departments.parent_department_number) SELECT * FROM tree order by path;");
  }

  /// Возвращает информацию об указанном подразделении
  static Future<List<Map<String, Map<String, dynamic>>>> one(int id) async {
    return await Database.connection.mappedResultsQuery('''
      SELECT departments.* FROM departments WHERE departments.number = @id;''',
        substitutionValues: {
          'id': id,
        });
  }

  static Future<void> add(Department department) async {
    await Database.connection.transaction((ctx) async {
      var id = await ctx.query(
          '''SELECT number FROM departments ORDER BY number DESC LIMIT 1;''');
      await ctx.query('''
      INSERT INTO departments (number, parent_department_number, name, inter_phone, phone, address, email, url) 
      VALUES (@number, @parent_department_number, @name, @inter_phone, @phone, @address, @email, @url);''',
          substitutionValues: {
            'number': id[0][0] + 1,
            'parent_department_number': department.parentDepartmentNumber,
            'name': department.name,
            'inter_phone': department.interPhone,
            'phone': department.phone,
            'address': department.address,
            'email': department.email,
            'url': department.url,
          });
    });
  }
}

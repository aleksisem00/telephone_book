import 'dart:convert';

import 'package:school_app/book/database/database.dart';
import 'package:school_app/book/database/entities/employee.dart';

const orderByList = [
  'fullname',
  'degree',
  'address',
];
const directions = [
  'ASC',
  'DESC',
];

class EmployeesRepository {
  static Future<List<Map<String, Map<String, dynamic>>>> all(
      int orderBy, int orderDirection) async {
    return await Database.connection.mappedResultsQuery(
        'SELECT * from employees_fullname ORDER BY ${orderByList[orderBy]} ${directions[orderDirection]};');
  }

  static Future<List<Map<String, Map<String, dynamic>>>> find(
      String param, int orderBy, int orderDirection) async {
    return await Database.connection.mappedResultsQuery('''
            SELECT id, fullname, degree, address, email, inter_phone, phone 
            FROM employees_fullname 
            WHERE LOWER(fullname) LIKE LOWER(@value) OR 
              LOWER(degree) LIKE LOWER(@value) OR 
              LOWER(address) LIKE LOWER(@value) OR 
              LOWER(email) LIKE LOWER(@value) OR 
              inter_phone LIKE @value OR 
              phone LIKE @value ORDER BY @orderBy @direction;''',
        substitutionValues: {
          'value': '%$param%',
          'orderBy': orderByList[orderBy],
          'direction': directions[orderDirection],
        });
  }

  static Future<List<Map<String, Map<String, dynamic>>>> allFromDepartment(
      int departmentID) async {
    return await Database.connection.mappedResultsQuery(
      '''SELECT departments.number, positions.*, employees_positions.*, employees.* 
          FROM departments
          RIGHT JOIN positions ON positions.department_number = departments.number
          RIGHT JOIN employees_positions ON employees_positions.position_code = positions.code
          RIGHT JOIN employees ON employees.id = employees_positions.employee_id 
          WHERE departments.number = @id;''',
      substitutionValues: {
        'id': departmentID,
      },
    );
  }

  static Future<List<Map<String, Map<String, dynamic>>>> getOne(int id) async {
    return await Database.connection
        .mappedResultsQuery('''SELECT encode(photo::bytea, 'escape'), employees.*, employees_positions.*, positions.*, departments.*  
          FROM employees 
          LEFT JOIN employees_positions ON employees.id = employees_positions.employee_id
          LEFT JOIN positions ON positions.code = employees_positions.position_code 
          LEFT JOIN departments ON departments.number = positions.department_number 
          WHERE id = @id;''', substitutionValues: {
      'id': id,
    });
  }

  static Future<void> add(Employee employee) async {
    return await Database.connection.transaction((ctx) async {
      var employeeID = await ctx
          .query('''INSERT INTO employees (firstname, lastname, patronymic, degree, address, email, inter_phone, phone, photo) 
        VALUES (@firstname, @lastname, @patronymic, @degree, @address, @email, @interPhone, @phone, @photo) RETURNING id;''',
              substitutionValues: {
            'firstname': employee.firstname,
            'lastname': employee.lastname,
            'patronymic': employee.patronymic,
            'degree': employee.degree,
            'address': employee.address,
            'email': employee.email,
            'interPhone': employee.interPhone,
            'phone': employee.phone,
            'photo': employee.photo,
          });
      String positions = '';
      for (var position in employee.positions) {
        if (positions.isNotEmpty) positions += ', ';
        positions +=
            '(${employeeID[0][0]}, ${position[1]["positions"]["code"]})';
      }
      await ctx.query(
          '''INSERT INTO employees_positions (employee_id, position_code) 
              VALUES ''' +
              positions +
              ';');
    });
  }

  static Future<void> edit(Employee employee) async {
    return await Database.connection.transaction((ctx) async {
      await ctx
          .query('''DELETE FROM employees_positions WHERE employee_id = @id''',
              substitutionValues: {
            'id': employee.id,
          });
      String positions = '';
      for (var position in employee.positions) {
        if (positions.isNotEmpty) positions += ', ';
        positions += '(${employee.id}, ${position[1]["positions"]["code"]})';
      }
      await ctx.query(
          '''INSERT INTO employees_positions (employee_id, position_code) 
              VALUES ''' +
              positions +
              ';');
      await ctx.query('''UPDATE public.employees
        SET firstname=@firstname, lastname=@lastname, patronymic=@patronymic, degree=@degree, address=@address, email=@email, inter_phone=@interPhone, phone=@phone, photo=@photo
        WHERE id = @id;''', substitutionValues: {
        'firstname': employee.firstname,
        'lastname': employee.lastname,
        'patronymic': employee.patronymic,
        'degree': employee.degree,
        'address': employee.address,
        'email': employee.email,
        'interPhone': employee.interPhone,
        'phone': employee.phone,
        'photo': employee.photo,
        'id': employee.id,
      });
    });
  }

  static Future<void> delete(Employee employee) async {
    return await Database.connection.transaction((ctx) async {
      await ctx
          .query('''DELETE FROM employees_positions WHERE employee_id = @id;''',
              substitutionValues: {
            'id': employee.id,
          });
      await ctx.query('''DELETE FROM employees WHERE id = @id;''',
          substitutionValues: {
            'id': employee.id,
          });
    });
  }
}

import 'package:school_app/book/database/database.dart';

class PositionsRepository {
  /// Возвращает дерево всех подразделений
  static Future<List<Map<String, Map<String, dynamic>>>> all() async {
    return await Database.connection
        .mappedResultsQuery('''SELECT * FROM positions;''');
  }
}

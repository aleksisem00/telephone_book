import 'package:school_app/book/database/database.dart';

class UsersRepository {
  static Future<Map<String, dynamic>> find(String login) async {
    var user = await Database.connection
        .mappedResultsQuery('''SELECT * FROM find_user('$login');''');
    return user[0]['']!;
  }
}

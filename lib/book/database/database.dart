import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:postgres/postgres.dart';

class Database {
  static late PostgreSQLConnection connection;
  static late String host;
  static late int port;
  static late String database;
  static late String username;
  static late String password;

  static Future<void> init() async {
    await dotenv.load(fileName: '.env');
    Database.host = dotenv.get('HOST');
    Database.port = int.parse(dotenv.get('PORT'));
    Database.database = dotenv.get('DATABASE');
    Database.username = dotenv.get('USERNAME');
    Database.password = dotenv.get('PASSWORD');
  }

  static Future<void> reconnect(String username, String password) async {
    Database.connection.close();
    Database.connection = PostgreSQLConnection(
        Database.host, Database.port, Database.database,
        username: username, password: password);
    await Database.connection.open();
  }
}

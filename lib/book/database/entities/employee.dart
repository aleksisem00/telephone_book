import 'dart:typed_data';

class Employee {
  final int id;
  String firstname;
  String lastname;
  String? patronymic;
  String? degree;
  String? address;
  String? email;
  String? interPhone;
  String? phone;
  List<dynamic> positions;
  Uint8List? photo;

  Employee(
      this.id,
      this.firstname,
      this.lastname,
      this.patronymic,
      this.degree,
      this.address,
      this.email,
      this.interPhone,
      this.phone,
      this.positions,
      this.photo);
}

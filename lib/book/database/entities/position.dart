class Position {
  final int code;
  final int departmentNumber;
  final String name;

  Position(this.code, this.departmentNumber, this.name);
}

class Department {
  final int number;
  final int? parentDepartmentNumber;
  final String name;
  final String? interPhone;
  final String? phone;
  final String address;
  final String? email;
  final String? url;

  Department(
    this.number,
    this.parentDepartmentNumber,
    this.name,
    this.interPhone,
    this.phone,
    this.address,
    this.email,
    this.url,
  );
}

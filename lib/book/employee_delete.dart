import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/database/entities/employee.dart';
import 'package:school_app/book/database/repos/employees.dart';

class EmployeeDeleteDialog extends StatelessWidget {
  final Employee employee;
  const EmployeeDeleteDialog({Key? key, required this.employee})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContentDialog(
      title: Text('Удалить сотрудника'),
      content: Text(
          'Вы действительно хотите удалить запись о сотруднике, а также записи о назначенных ему должностях?'),
      actions: [
        Button(
          child: const Text('Отмена'),
          onPressed: () => Navigator.pop(context, null),
        ),
        FilledButton(
          child: const Text('Удалить'),
          onPressed: () {
            deleteEmployee();
            Navigator.pop(context, employee);
          },
        ),
      ],
    );
  }

  void deleteEmployee() async {
    await EmployeesRepository.delete(employee);
  }
}

import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/database/repos/employees.dart';
import 'package:school_app/book/employee_detailed.dart';
import 'package:school_app/book/employee_add.dart';

class EmployeesPage extends StatefulWidget {
  const EmployeesPage({Key? key}) : super(key: key);

  @override
  State<EmployeesPage> createState() => _EmployeesPageState();
}

class _EmployeesPageState extends State<EmployeesPage> {
  late List<Map<String, Map<String, dynamic>>> _employees;

  final searchController = TextEditingController();
  final sortRadioButtons = <String>[
    'ФИО',
    'Учёная степень',
    'Адрес',
  ];
  int _sortingIndex = 0;
  final directionsRadioButtons = <String>[
    'По возрастанию',
    'По убыванию',
  ];
  int _directionIndex = 0;

  FocusNode searchFocusNode = FocusNode();

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    loadEmployees();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldPage(
      header: const Text('Сотрудники'),
      content: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 8),
            CommandBar(primaryItems: [
              CommandBarButton(
                  icon: const Icon(FluentIcons.add),
                  label: const Text('Добавить сотрудника'),
                  onPressed: () => showDialog(
                          context: context,
                          builder: (context) => const EmployeeForm())
                      .then((value) => loadEmployees())),
            ]),
            const SizedBox(height: 8),
            SizedBox(
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 400,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text('Сортировать: '),
                            ...List.generate(
                                sortRadioButtons.length,
                                (index) => RadioButton(
                                      checked: _sortingIndex == index,
                                      onChanged: (value) {
                                        setState(() => _sortingIndex = index);
                                        (searchController.text.isNotEmpty)
                                            ? searchEmployee(
                                                searchController.text)
                                            : loadEmployees();
                                      },
                                      content: Text(sortRadioButtons[index]),
                                    ))
                          ]),
                    ),
                    SizedBox(
                      width: 270,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: List.generate(
                            directionsRadioButtons.length,
                            (index) => RadioButton(
                                  checked: _directionIndex == index,
                                  onChanged: (value) {
                                    setState(() => _directionIndex = index);
                                    (searchController.text.isNotEmpty)
                                        ? searchEmployee(searchController.text)
                                        : loadEmployees();
                                  },
                                  content: Text(directionsRadioButtons[index]),
                                )),
                      ),
                    ),
                  ],
                )),
            const SizedBox(height: 8),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: TextBox(
                    onSubmitted: (text) {
                      searchEmployee(text);
                      searchFocusNode.requestFocus();
                    },
                    controller: searchController,
                    focusNode: searchFocusNode,
                    placeholder:
                        'ФИО, должность, подразделение, адрес, контактная информация',
                    suffix: IconButton(
                        icon: const Icon(FluentIcons.search),
                        onPressed: () {
                          searchEmployee(searchController.text);
                          searchFocusNode.requestFocus();
                        }),
                  ),
                ),
              ],
            ),
            Expanded(
              child: EmployeesList(
                employees: _employees.map((e) => e['']!).toList(),
                updateEmployeesList: loadEmployees,
              ),
            )
          ],
        ),
      ),
    );
  }

  void searchEmployee(String param) async {
    var employees =
        await EmployeesRepository.find(param, _sortingIndex, _directionIndex);
    setState(() {
      _employees = employees;
    });
  }

  void loadEmployees() async {
    var employees =
        await EmployeesRepository.all(_sortingIndex, _directionIndex);
    setState(() {
      _employees = employees;
    });
  }
}

class EmployeesList extends StatelessWidget {
  final List<Map<String, dynamic>> employees;
  final Function() updateEmployeesList;
  const EmployeesList(
      {Key? key, required this.employees, required this.updateEmployeesList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ...employees.map((e) => EmployeeCard(
              employee: e,
              updateEmployeesList: updateEmployeesList,
            ))
      ],
    );
  }
}

class EmployeeCard extends StatelessWidget {
  final Map<String, dynamic> employee;
  final Function() updateEmployeesList;
  const EmployeeCard(
      {Key? key, required this.employee, required this.updateEmployeesList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8.0),
      width: double.infinity,
      child: GestureDetector(
        onTap: () => Navigator.of(context)
            .push(FluentPageRoute(
                builder: (context) =>
                    EmployeeDetailedPage(employeeID: employee['id'])))
            .then((value) => updateEmployeesList()),
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              employee['fullname'] != null
                  ? Text(employee["fullname"])
                  : Text(employee['lastname'] +
                      ' ' +
                      employee['firstname'] +
                      ' ' +
                      employee['patronymic']),
              if (employee['degree'].isNotEmpty) Text(employee['degree']),
              if (employee['address'].isNotEmpty)
                Text("Адрес: " + employee['address']),
              if (employee['email'].isNotEmpty)
                Text("Email: " + employee['email']),
              if (employee['phone'].isNotEmpty)
                Text("Тел.: " + employee['phone']),
              if (employee['inter_phone'].isNotEmpty)
                Text("Внут.: " + employee['inter_phone']),
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/database/entities/department.dart';
import 'package:school_app/book/database/entities/employee.dart';
import 'package:school_app/book/database/entities/position.dart';
import 'package:school_app/book/database/repos/departments.dart';
import 'package:school_app/book/database/repos/employees.dart';
import 'package:school_app/book/database/repos/positions.dart';
import 'package:school_app/book/positions_list.dart';

class EmployeeEditForm extends StatefulWidget {
  final Employee employee;
  const EmployeeEditForm({Key? key, required this.employee}) : super(key: key);

  @override
  State<EmployeeEditForm> createState() => _EmployeeEditFormState();
}

class _EmployeeEditFormState extends State<EmployeeEditForm> {
  late Employee _employee;

  final _controller = ScrollController(keepScrollOffset: true);
  final _firstname = TextEditingController();
  final _lastname = TextEditingController();
  final _patronymic = TextEditingController();
  final _degree = TextEditingController();
  final _address = TextEditingController();
  final _email = TextEditingController();
  final _interPhone = TextEditingController();
  final _phone = TextEditingController();

  List<dynamic> _positionsChosen = [];
  File? _photo;
  Map<String, Map<String, dynamic>>? comboBoxPosition;
  Map<String, Map<String, dynamic>>? comboBoxDepartment;

  List<Map<String, Map<String, dynamic>>>? _departments = [];
  List<Map<String, Map<String, dynamic>>>? _positions = [];
  List<Map<String, Map<String, dynamic>>>? _positionsFiltered = [];

  void loadPositions() async {
    var positions = await PositionsRepository.all();
    var departments = await DepartmentsRepository.all();
    setState(() {
      _positions = positions;
      _departments = departments;
      _positionsFiltered = positions;
    });
  }

  void removePosition(List<dynamic> position) {
    _positionsChosen.remove(position);
    List<Map<String, Map<String, dynamic>>> positions = [
      ..._positions!,
      position[1]
    ];
    List<Map<String, Map<String, dynamic>>> departments = [
      ..._departments!,
      position[0]
    ];
    setState(() {
      _positions = positions;
      _departments = departments;
    });
  }

  void chooseDepartment(Map<String, Map<String, dynamic>> value) {
    setState(() {
      comboBoxDepartment = value;
      _positionsFiltered = _positions!
          .where((e) =>
              e['positions']!['department_number'] == value['']!['number'])
          .toList();
    });
  }

  void choosePhoto() async {
    FilePickerResult? result =
        await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null) {
      setState(() {
        _photo = File(result.files.single.path!);
      });
    }
  }

  void addPosition() {
    if (!(comboBoxPosition != null && comboBoxDepartment != null)) return;
    var positionsList = [comboBoxDepartment, comboBoxPosition];
    setState(() {
      _positionsChosen = [..._positionsChosen, positionsList];
      _positions!.remove(comboBoxPosition!);
      _departments!.remove(comboBoxDepartment);
    });
    comboBoxDepartment = null;
    comboBoxPosition = null;
  }

  void editEmployee() {
    if (_firstname.text.trim().isNotEmpty &&
        _lastname.text.trim().isNotEmpty &&
        _positionsChosen.isNotEmpty) {
      _employee.firstname = _firstname.text;
      _employee.lastname = _lastname.text;
      _employee.patronymic = _patronymic.text;
      _employee.degree = _degree.text;
      _employee.address = _address.text;
      _employee.email = _email.text;
      _employee.interPhone = _interPhone.text;
      _employee.phone = _phone.text;
      _employee.positions = _positionsChosen;
      _employee.photo = (_photo != null) ? _photo!.readAsBytesSync() : null;
      EmployeesRepository.edit(_employee).then((value) {
        Navigator.pop(context);
        showSnackbar(
            context, const Snackbar(content: Text('Сотрудник был изменён')));
      });
    }
  }

  void initTextEditors() async {
    var photo = (_employee.photo != null)
        ? await File('image.jpg').writeAsBytes(_employee.photo!)
        : null;
    setState(() {
      _firstname.text = _employee.firstname;
      _lastname.text = _employee.lastname;
      _patronymic.text = _employee.patronymic ?? '';
      _degree.text = _employee.degree ?? '';
      _address.text = _employee.address ?? '';
      _email.text = _employee.email ?? '';
      _interPhone.text = _employee.interPhone ?? '';
      _phone.text = _employee.phone ?? '';
      _photo = photo;
    });
  }

  void initPositions() async {
    var positions = await PositionsRepository.all();
    var departments = await DepartmentsRepository.all();
    List<Map<String, Map<String, dynamic>>> positionsList = [];
    List<dynamic> positionsChosen = [];
    for (var i in positions) {
      for (var j in _employee.positions) {
        if (i['positions']!['code'] != (j[0] as Position).code) {
          positionsList.add(i);
          break;
        } else {
          positionsChosen.add([
            departments.firstWhere((element) =>
                element['']!['number'] == i['positions']!['department_number']),
            i
          ]);
          break;
        }
      }
    }
    List<Map<String, Map<String, dynamic>>> departmentsList = [];
    List<Map<String, Map<String, dynamic>>> departmentsChosen = [];

    for (var i in departments) {
      for (var j in _employee.positions) {
        if (i['']!['number'] != (j[1] as Department).number) {
          departmentsList.add(i);
          break;
        } else {
          departmentsChosen.add(i);
          break;
        }
      }
    }

    setState(() {
      _positions = positionsList;
      _departments = departmentsList;
      _positionsChosen = positionsChosen;
    });
  }

  @override
  void initState() {
    _employee = widget.employee;
    initTextEditors();
    initPositions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ContentDialog(
      title: const Text('Сотрудник'),
      constraints: const BoxConstraints(maxWidth: 800, maxHeight: 800),
      content: ScaffoldPage(
        content: SingleChildScrollView(
          controller: _controller,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormBox(
                controller: _lastname,
                header: 'Фамилия',
                placeholder: 'Иванов',
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (text) {
                  if (text == null || text.isEmpty) return 'Введите фамилию';
                  return null;
                },
              ),
              TextFormBox(
                controller: _firstname,
                header: 'Имя',
                placeholder: 'Иван',
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (text) {
                  if (text == null || text.isEmpty) return 'Введите имя';
                  return null;
                },
              ),
              TextFormBox(
                controller: _patronymic,
                header: 'Отчество',
                placeholder: 'Иванович',
              ),
              TextFormBox(
                controller: _degree,
                header: 'Ученая степень',
                placeholder: 'Кандидат технических наук',
              ),
              TextFormBox(
                controller: _address,
                header: 'Адрес',
                placeholder: 'Адрес',
              ),
              TextFormBox(
                controller: _email,
                header: 'Email',
                placeholder: 'iii@tusur.ru',
              ),
              TextFormBox(
                controller: _interPhone,
                header: 'Внутр.',
                placeholder: '2222',
              ),
              TextFormBox(
                controller: _phone,
                header: 'Телефон',
                placeholder: '(222) 222-22-22',
              ),
              const SizedBox(
                height: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Подразделение'),
                  Combobox<Map<String, Map<String, dynamic>>>(
                    placeholder: const Text('Подразделение'),
                    isExpanded: true,
                    items: _departments!
                        .map((e) =>
                            ComboboxItem<Map<String, Map<String, dynamic>>>(
                              value: e,
                              child: Text(e['']!['name']),
                            ))
                        .toList(),
                    value: comboBoxDepartment,
                    onChanged: (value) {
                      comboBoxDepartment = null;
                      comboBoxPosition = null;
                      if (value != null) chooseDepartment(value);
                    },
                    icon: const Icon(FluentIcons.add),
                  ),
                  const Text('Должность'),
                  Combobox<Map<String, Map<String, dynamic>>>(
                    placeholder: const Text('Должности'),
                    isExpanded: true,
                    items: (comboBoxDepartment != null)
                        ? _positionsFiltered!
                            .map((e) =>
                                ComboboxItem<Map<String, Map<String, dynamic>>>(
                                  value: e,
                                  child: Text(e['positions']!['name']),
                                ))
                            .toList()
                        : [],
                    value: comboBoxPosition,
                    onChanged: (value) {
                      if (value != null)
                        setState(() => comboBoxPosition = value);
                    },
                    icon: const Icon(FluentIcons.add),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Button(
                    child: const Text('Добавить'),
                    onPressed: () => addPosition(),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  PositionsList(
                      positionsChosen: _positionsChosen,
                      removePosition: removePosition),
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              const Text('Фото'),
              Row(
                children: [
                  if (_photo != null) ...[
                    SizedBox(
                      width: 180,
                      height: 250,
                      child: Image.file(_photo!),
                    ),
                  ] else ...[
                    const SizedBox(
                      width: 180,
                      height: 250,
                    )
                  ],
                  const SizedBox(
                    width: 50,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Button(
                          child: const Text('Выбрать...'),
                          onPressed: () => choosePhoto()),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      actions: [
        Button(
          child: const Text('Отмена'),
          onPressed: () => Navigator.pop(context),
        ),
        FilledButton(
          child: const Text('Изменить'),
          onPressed: () => editEmployee(),
        ),
      ],
    );
  }
}

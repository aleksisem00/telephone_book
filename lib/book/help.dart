import 'package:fluent_ui/fluent_ui.dart';

class HelpPage extends StatelessWidget {
  const HelpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScaffoldPage(
      header: const Text('Справка'),
      content: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: const [
            Text('Здесь будет отображаться справочная информация.'),
          ],
        ),
      ),
      // content: Column(
      //   children: const [
      //     Text('Здесь будет отображаться справочная информация.'),
      //   ],
      // ),
    );
  }
}

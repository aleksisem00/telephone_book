import 'dart:ui';

import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/database/repos/departments.dart';
import 'package:school_app/book/database/repos/employees.dart';
import 'package:school_app/book/employees.dart';

class DepartmentPage extends StatefulWidget {
  final int departmentID;

  const DepartmentPage({Key? key, required this.departmentID})
      : super(key: key);

  @override
  State<DepartmentPage> createState() => _DepartmentPageState();
}

class _DepartmentPageState extends State<DepartmentPage> {
  late List<Map<String, Map<String, dynamic>>> _departmentData;
  late List<Map<String, Map<String, dynamic>>>? _employeesData;

  @override
  void initState() {
    loadDepartment();
    loadEmployees();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return NavigationView(
      appBar: NavigationAppBar(
        title: const Text('Департамент'),
      ),
      content: ScaffoldPage(
        content: ListView(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 100),
          children: [
            Text(
              _departmentData[0]['departments']!['name'],
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 8,
            ),
            Table(
              border: TableBorder(
                bottom: BorderSide(color: Colors.black),
              ),
              children: [
                TableRow(children: [
                  Text(
                    'Контактная информация:',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  )
                ]),
                TableRow(children: [
                  SizedBox(
                    height: 8,
                  )
                ]),
                if (_departmentData[0]['departments']!['address'] != null)
                  TableRow(
                    children: [
                      Text('Адрес: ' +
                          _departmentData[0]['departments']!['address'])
                    ],
                  ),
                if (_departmentData[0]['departments']!['phone'] != null)
                  TableRow(
                    children: [
                      Text('Тел.: ' +
                          _departmentData[0]['departments']!['phone'])
                    ],
                  ),
                if (_departmentData[0]['departments']!['inter_phone'] != null)
                  TableRow(
                    children: [
                      Text('Внутр.: ' +
                          _departmentData[0]['departments']!['inter_phone'])
                    ],
                  ),
                if (_departmentData[0]['departments']!['email'] != null)
                  TableRow(
                    children: [
                      Text('Email: ' +
                          _departmentData[0]['departments']!['email'])
                    ],
                  ),
                if (_departmentData[0]['departments']!['url'] != null)
                  TableRow(
                    children: [
                      Text('Сайт: ' + _departmentData[0]['departments']!['url'])
                    ],
                  ),
                TableRow(children: [
                  SizedBox(
                    height: 8,
                  )
                ]),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            if (_employeesData != null && _employeesData!.isNotEmpty) ...[
              Text(
                'Сотрудники:',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              ..._employeesData!
                  .map((e) => EmployeeCard(
                        employee: e["employees"]!,
                        updateEmployeesList: loadEmployees,
                      ))
                  .toList()
              // EmployeesList(
              //     employees:
              //         _employeesData!.map((e) => e['employees']!).toList()),
            ],
          ],
        ),
      ),
    );
  }

  void loadDepartment() async {
    var departmentData = await DepartmentsRepository.one(widget.departmentID);
    setState(() {
      _departmentData = departmentData;
    });
  }

  void loadEmployees() async {
    var employees =
        await EmployeesRepository.allFromDepartment(widget.departmentID);
    setState(() {
      _employeesData = employees;
    });
  }
}

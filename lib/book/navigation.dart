import 'package:fluent_ui/fluent_ui.dart';
import 'package:school_app/book/disciplines.dart';
import 'package:school_app/book/employees.dart';
import 'package:school_app/book/help.dart';
import 'package:school_app/book/home.dart';
import 'package:school_app/login.dart';
import 'package:school_app/storage.dart';
import 'package:school_app/user/role.dart';
import 'package:school_app/user/user.dart';

class NavigationPanel extends StatefulWidget {
  const NavigationPanel({
    Key? key,
  }) : super(key: key);

  @override
  State<NavigationPanel> createState() => _NavigationPanelState();
}

class _NavigationPanelState extends State<NavigationPanel> {
  late User user;
  late List<Widget> _widgetOptions;
  bool _isUserChanged = false;

  int _index = 0;

  void changeUser() {
    user = Storage.getUser();
    setState(() {
      _isUserChanged = !_isUserChanged;
      _widgetOptions = (user.role == Role.admin)
          ? [
              Home(),
              EmployeesPage(),
              HelpPage(),
            ]
          : [
              Home(),
              LoginForm(changeUser: changeUser),
              HelpPage(),
            ];
    });
  }

  @override
  void initState() {
    user = Storage.getUser();
    _widgetOptions = (user.role == Role.admin)
        ? [
            Home(),
            EmployeesPage(),
            HelpPage(),
          ]
        : [
            Home(),
            LoginForm(changeUser: changeUser),
            HelpPage(),
          ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return NavigationView(
        appBar: const NavigationAppBar(
          automaticallyImplyLeading: false,
          title: Text('Телефонный справочник'),
        ),
        pane: NavigationPane(
          selected: _index,
          onChanged: (i) => setState(() {
            _index = i;
          }),
          displayMode: PaneDisplayMode.auto,
          indicator: const EndNavigationIndicator(),
          items: [
            PaneItem(
              icon: const Icon(FluentIcons.home),
              title: const Text('Главная'),
            ),
            if (user.role == Role.admin) ...[
              PaneItem(
                icon: const Icon(FluentIcons.people),
                title: const Text('Сотрудники'),
              ),
            ],
          ],
          footerItems: [
            if (user.role == Role.user)
              PaneItem(
                icon: const Icon(FluentIcons.add_friend),
                title: const Text('Войти'),
              ),
            PaneItem(
              icon: const Icon(FluentIcons.unknown),
              title: const Text('Справка'),
            ),
          ],
        ),
        content: Stack(
          children: Iterable<int>.generate(_widgetOptions.length)
              .map((i) => _buildOffstageNavigator(i))
              .toList(),
        ));
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, int index) {
    return {
      '/': (context) {
        return _widgetOptions.elementAt(index);
      }
    };
  }

  Widget _buildOffstageNavigator(int index) {
    var routeBuilders = _routeBuilders(context, index);
    return Offstage(
      offstage: _index != index,
      child: Navigator(
        onGenerateRoute: (routeSettings) {
          return FluentPageRoute(
              builder: (context) =>
                  routeBuilders[routeSettings.name]!(context));
        },
      ),
    );
  }
}

import 'package:school_app/user/user.dart';

class Storage {
  static User? _user;

  static getUser() {
    return _user;
  }

  static setUser(User newUser) {
    _user = newUser;
  }
}

import 'package:school_app/user/role.dart';

class User {
  String? login;
  Role role;

  User(this.login, this.role);
}

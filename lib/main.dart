// ignore_for_file: prefer_const_constructors

import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:postgres/postgres.dart';
import 'package:school_app/book/database/database.dart';
import 'package:school_app/book/navigation.dart';
import 'package:school_app/storage.dart';
import 'package:school_app/user/role.dart';
import 'package:school_app/user/user.dart';

void main() async {
  await Database.init().then((value) async {
    Database.connection = PostgreSQLConnection(
        Database.host, Database.port, Database.database,
        username: Database.username, password: Database.password);
    await Database.connection.open();
    runApp(const Book());
  });
}

class Book extends StatefulWidget {
  const Book({Key? key}) : super(key: key);

  @override
  State<Book> createState() => _BookState();
}

class _BookState extends State<Book> {
  final title = 'Телефонный справочник';
  @override
  Widget build(BuildContext context) {
    Storage.setUser(User(null, Role.user));
    return FluentApp(
        // theme: ThemeData(
        //   focusTheme: const FocusThemeData(glowFactor: 4.0),
        // ),
        home: NavigationPanel(),
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
          accentColor: Colors.blue,
        ));
  }
}
